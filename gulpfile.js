var gulp         = require('gulp');
var pug          = require('gulp-pug');
var stylus       = require('gulp-stylus');
var plumber      = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var browserSync  = require('browser-sync').create();



gulp.task('styles', () => {

  return gulp.src('./source/styl/main.styl')
    .pipe( plumber() )
    .pipe( stylus() )
    .pipe( autoprefixer() )
    .pipe( gulp.dest('dev/css'));

});



gulp.task('page', () => {

  return gulp.src('./source/page/index.pug')
    .pipe( plumber() )
    .pipe( pug({pretty: true}) )
    .pipe( gulp.dest('dev/'));

});



gulp.task('server', () => {

  browserSync.init({
    server: 'dev'
  });

  browserSync.watch('dev/**/*.*').on('change', browserSync.reload);

});



gulp.task('watch', () => {
  gulp.watch('./source/styl/**/*', ['styles']);
  gulp.watch('./source/page/**/*', ['page']);
});



gulp.task('default', ['styles','page','watch']);